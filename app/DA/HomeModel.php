<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class HomeModel
{
  public static function getData()
  {
    $panjar_beredar=$total_panjar=$balik_nota=0;
    foreach(DB::table('panjar_master')->get() as $d){
        if($d->step_id==4){
            $total_panjar+=$d->tf_finance;
            $balik_nota+=$d->nominal_nota;
            $panjar_beredar+=($d->tf_finance-$d->nominal_nota);
        }
    }
    $donut =  [
        [ 'Total Panjar', $total_panjar ],
        [ 'Panjar Beredar', $panjar_beredar ],
        [ 'Balik Nota/Cash', $balik_nota ]
      ];
    $horisontal = [['x','Panjar Beredar','Balik Nota/Cash','Total Panjar'],
              ["value",$panjar_beredar,$balik_nota,$total_panjar]];
    return ['donut'=>$donut,'horisontal'=>$horisontal];
  }

}
