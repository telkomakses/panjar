<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class LogModel
{
    private static function getQuery(){
        return DB::table('panjar_log')->leftJoin('panjar_step','panjar_log.step_id', '=', 'panjar_step.id')->leftJoin('panjar_master', 'panjar_log.panjar_id','=', 'panjar_master.id');
    }
    public static function getAllByPanjarId($id)
    {
        return self::getQuery()->where('panjar_log.panjar_id', $id)->get();
    }
    public static function getByPanjarId($id)
    {
        return self::getQuery()->where('panjar_log.panjar_id', $id)->orderBy('panjar_log.id', 'desc')->first();
    }
    public static function getById($id)
    {
        return self::getQuery()->where('panjar_log.panjar_id', $id)->first();
    }
    public static function insertLog($insert){
        $last = self::getByPanjarId($insert['panjar_id']);
        if($last){
            $insert['start_date'] = $last->end_date;
        }else{
            $insert['start_date'] = DB::raw('now()');
        }
        DB::table('panjar_log')->insert($insert);
    }
}
