<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class PanjarModel
{
    private static function getQuery(){
        return DB::table('panjar_master')->select('panjar_master.*', 'panjar_step.step','panjar_step.backward','panjar_step.forward')->leftJoin('panjar_step','panjar_master.step_id', '=', 'panjar_step.id');
    }
    public static function getByStepId($id)
    {
        return self::getQuery()->where('panjar_master.step_id', $id)->get();
    }
    public static function getAll()
    {
        return self::getQuery()->get();
    }
    public static function getById($id)
    {
        return self::getQuery()->where('panjar_master.id', $id)->first();
    }
    public static function insertMaster($param){
        return DB::table('panjar_master')->insertGetId($param);
    }
    public static function updateMaster($id, $param){
        DB::table('panjar_master')->where('id', $id)->update($param);
    }
}