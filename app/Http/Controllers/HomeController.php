<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\HomeModel;
use App\DA\PanjarModel;
use Excel;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function home()
    {   
        $data = HomeModel::getData();
        return view('home', compact('data'));
    }
    public function rekap()
    {   
        $data = PanjarModel::getAll();
        return view('panjar.rekap', compact('data'));
    }

}