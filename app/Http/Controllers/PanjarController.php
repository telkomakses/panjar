<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\PanjarModel;
use App\DA\LogModel;
// use App\DA\ProjectModel;
// use Excel;

use Illuminate\Support\Facades\Session;

class PanjarController extends Controller
{
    public function progress($id)
    {   
        $data = PanjarModel::getById($id);
        // dd($data);
        switch ($data->step_id) {
        case 1:
            return view('panjar.request_panjar', compact('data'));
            break;
        case 2:
            return view('panjar.approval', compact('data'));
            break;
        case 3:
            return view('panjar.tf', compact('data'));
            break;
        case 4:
            return view('panjar.baliknota', compact('data'));
            break;
        default:
            dd('wrong step, back and reload.');
            break;
        }
    }
    public function list($stepid)
    {   
        $data = PanjarModel::getByStepId($stepid);
        return view('panjar.list',compact('data'));
    }
    public function create_panjar(){
        $data = new \stdClass();
        return view('panjar.request_panjar', compact('data'));
    }
    public function save_request_panjar(Request $req, $id){
        $auth = session('auth');
        $param = [
            'step_id' => 2,
            'nik'     => $auth->id_user,
            'nama'    => $auth->nama,
            'nominal' => $req->nominal,
            'keperluan' => $req->keperluan,
            'lama_balik_nota' => $req->lama_balik_nota
        ];
        if($id){
            //update
            PanjarModel::updateMaster($id, $param);
        }else{
            //insert
            $id = PanjarModel::insertMaster($param);
        }
        LogModel::insertLog(['panjar_id'=>$id,'catatan'=>'','create_by'=>$auth->id_user,'step_id'=>1]);
        return redirect('/list/1');
    }
    public function save_approval(Request $req, $id){
        $auth = session('auth');
        PanjarModel::updateMaster($id, ['step_id'=>$req->status_approval,'prioritas'=>$req->prioritas]);
        LogModel::insertLog(['panjar_id'=>$id,'catatan'=>$req->catatan,'create_by'=>$auth->id_user,'step_id'=>2]);
        return redirect('/list/2');
    }
    public function save_tf(Request $req, $id){
        $auth = session('auth');
        PanjarModel::updateMaster($id, ['step_id'=>$req->status, 'tf_finance'=>$req->tf_finance]);
        LogModel::insertLog(['panjar_id'=>$id,'catatan'=>$req->catatan,'create_by'=>$auth->id_user,'step_id'=>3]);
        return redirect('/list/3');
    }
    public function save_balik_nota(Request $req, $id){
        $auth = session('auth');
        $step_id=4;
        if($req->lunas){
            $step_id=5;
        }
        $data = PanjarModel::getById($id);
        PanjarModel::updateMaster($id, ['step_id'=>$step_id, 'nominal_nota'=>($data->nominal_nota+$req->nominal_nota)]);
        LogModel::insertLog(['panjar_id'=>$id,'catatan'=>$req->catatan,'create_by'=>$auth->id_user,'step_id'=>4,'nominal_nota'=>$req->nominal_nota]);
        return redirect('/list/4');
    }
}