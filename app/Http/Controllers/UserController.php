<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class UserController extends Controller
{
	public function login(Request $request)
  {
    $username  = $request->input('username');
    $password = $request->input('password');
    $result = DB::select('
      SELECT u.id_user, u.password, u.id_karyawan, k.nama, u.level, u.promise_level,u.pixeltheme
      FROM user u
      LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
      WHERE id_user = ? AND password = MD5(?)
      ', [
        $username,
        $password
      ]);
    if (count($result)) {
      
      Session::put('auth', $result[0]);
      return redirect($result[0]->promise_level==1?'/':'/');  
    }else{
    	return redirect()->back()->with('alertblock', [
        ['type' => 'danger', 'text' => 'Gagal Login']
      ]);
    }
  }
  public function logout()
  {
    Session::forget('auth');
    return redirect('/login');
  }
  public function themes()
  {
    
    return view('theme');
  }
  public function themeSave(Request $req )
  {
    
    session('auth')->pixeltheme=$req->theme;
    DB::table('user')->where('id_user', session('auth')->id_user)->update([
      'pixeltheme'  => $req->theme
    ]);
    return back();
  }
}