@extends('layout')
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="tematik" class=" pull-right" id="filter"/>
    </div>  
</div>
@endsection
@section('css')
<style type="text/css">
  tr > th {
    text-align: center;
  }
</style>
@endsection
@section('title', 'Dashboard')
@section('content')
<div id="c3-donut" style="height: 250px" class="col-sm-6"></div>
<div id="horisontalbar2" style="height: 250px" class="col-sm-6"></div>
@endsection
@section('js')
<script>
  $(function() {
    var data = {
      columns: <?= json_encode($data['donut']); ?>,
      type : 'donut',
    };

    c3.generate({
      bindto: '#c3-donut',
      color: { pattern: [ '#673AB7', '#E040FB', '#D32F2F', '#9E9E9E', '#0288D1' ] },
      data: data,
      donut: { title: 'Some title' },
    });
    var horisontalbar2 = c3.generate({
        bindto: '#horisontalbar2',
        data: {
            x: 'x',
            labels: {
              format: function (v, id, i, j) { return "Rp. "+d3.format(",")(v) },
            },
            columns:<?= json_encode($data['horisontal']); ?>,
            type: 'bar',
        },
        bar: {
            width: 30
        },
        axis: {
            rotated: true,
            y: { 
                        show: false,
                    },
            x: {
                type: 'category'
            }
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
    });
  });
</script>
@endsection