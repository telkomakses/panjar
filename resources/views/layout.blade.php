<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">

   <?php
        $auth = session('auth') ?: '';
        $path = Request::path();
        $theme = 'mint-dark';
        if(isset($auth->pixeltheme)){
            if($auth->pixeltheme){
                $theme = $auth->pixeltheme;
            }
        }
        $color = '';
        if (str_contains($theme, 'dark')) {
            $color = '-dark';
        }
    ?>
    <link href="/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">

    <script src="/pace/pace.min.js"></script>
    @yield('css')
</head>
<body>
    <?php
    $admin = array('97150038', '17920262');

    ?>
    <nav class="px-nav px-nav-left px-nav-fixed">
        <button type="button" class="px-nav-toggle" data-toggle="px-nav">
            <span class="px-nav-toggle-arrow"></span>
            <span class="navbar-toggle-icon"></span>
            <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
        </button>

        <ul class="px-nav-content">
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-ios-home"></i><span class="px-nav-label">Dashboard</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/"><span class="px-nav-label">Home</span></a></li>
                    <li class="px-nav-item"><a href="/rekap"><span class="px-nav-label">Rekap</span></a></li>
                </ul>
            </li>
            <li class="px-nav-item">
                <a href="/list/1"><i class="px-nav-icon ion-compose"></i><span
                class="px-nav-label">Request Panjar</span></a>
            </li> 
            <li class="px-nav-item">
                <a href="/list/2"><i class="px-nav-icon ion-android-checkbox-outline"></i><span
                class="px-nav-label">Approval</span></a>
            </li> 
            <li class="px-nav-item">
                <a href="/list/3"><i class="px-nav-icon ion-cash"></i><span
                class="px-nav-label">Cash&Bank</span></a>
            </li> 
            <li class="px-nav-item">
                <a href="/list/4"><i class="px-nav-icon ion-arrow-return-left"></i><span
                class="px-nav-label">Balik Nota/Cash</span></a>
            </li>
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-person"></i><span class="px-nav-label">{{ session('auth')->nama }} <code>{{ session('auth')->id_user }}</code></span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/theme"><span class="px-nav-label">Themes</span></a></li>
                    <li class="px-nav-item"><a href="/logout"><span class="px-nav-label">Logout</span></a></li>
                </ul>
            </li>
        </ul>
    </nav>

    <nav class="navbar px-navbar">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Telkomakses</a>
        </div>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

        <div class="collapse navbar-collapse" id="px-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form class="navbar-form" role="search" action="/search">
                        <div class="form-group">
                            <input type="text" name="q" class="form-control" placeholder="Search PID/LOP" style="width: 240px;">
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="px-content">
        <div class="page-header">
            @yield('heading')
        </div>
        <div id="block-alert-with-timer" class="m-b-1"></div>
        <div>
            @yield('content')

        </div>
    </div>

    <footer class="px-footer px-footer-bottom">
        Powered by Laravel-Pixeladmin.
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/pixeladmin.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var alertExist = <?= json_encode(Session::has('alerts')); ?>;
            if(alertExist){
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "timeOut": "200000"
                }
                var msg = <?= json_encode(session('alerts')[0]); ?>;
                toastr.info(msg.text);
            }

            var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
            if(alertBlock){
                var $container = $('#block-alert-with-timer');
                var alrt = <?= json_encode(session('alertblock')[0]); ?>;
                $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
            }

            var file = window.location.pathname;

            $('body > .px-nav')
            .find('.px-nav-item > a[href="' + file + '"]')
            .parent()
            .addClass('active');
            
            $('body > .px-nav').pxNav();
            $('body > .px-footer').pxFooter();
        });
    </script>
    @yield('js')
</body>
</html>
