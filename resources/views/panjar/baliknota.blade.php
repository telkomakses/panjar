@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Form Balik Nota/Cash</span>
</h1>
@endsection
@section('title', 'Balik Nota')
@section('content')
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data" action="/save_balik_nota/{{ Request::segment(2) }}">
    <div class="form-group form-message-dark">
        <label class="col-md-2 control-label">TF/Cash</label>
        <div class="col-md-9">
            <input type="text" class="form-control" value="{{ number_format($data->tf_finance) ?: '' }}" disabled/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label class="col-md-2 control-label">Nominal Sudah Balik</label>
        <div class="col-md-9">
            <input type="text" class="form-control" value="{{ number_format($data->nominal_nota) ?: '' }}" disabled/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label class="col-md-2 control-label">Sisa</label>
        <div class="col-md-9">
            <input type="text" class="form-control" value="{{ number_format($data->tf_finance-$data->nominal_nota) ?: '' }}" disabled/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nominal_nota" class="col-md-2 control-label">Nominal Nota/Cash Balik</label>
        <div class="col-md-9">
            <input type="text" name="nominal_nota" id="nominal_nota" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-2 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea name="catatan" id="catatan" class="form-control" required></textarea>
        </div>
    </div>

    <div class="form-group form-message-dark">
        <label for="switcher-sm" class="col-md-2 control-label">Lunas?</label>
        <div class="col-md-9">
          <label for="switcher-sm" class="switcher switcher-blank switcher-success">
            <input type="checkbox" id="switcher-sm" name="lunas">
            <div class="switcher-indicator">
              <div class="switcher-yes">YES</div>
              <div class="switcher-no">NO</div>
            </div>
          </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
    });
</script>
@endsection
