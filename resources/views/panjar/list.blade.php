@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>List
</h1>
@if(Request::segment(2)==1)
<a href="/requestPanjar" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Request Panjar</span></a>
@endif
@endsection
@section('title', 'List')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Nominal</th>
                <th>Keperluan</th>
                <th>Lama Balik Nota</th>
                <th>Prioritas</th>
                <th>Act</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ number_format($d->nominal) }}</td>
                    <td>{{ $d->keperluan }}</td>
                    <td>{{ $d->lama_balik_nota }} Hari</td>
                    <td>{{ $d->prioritas }}</td>
                    <td>
                        <a href="/progress/{{ $d->id }}" class="btn btn-xs btn-success"><i class="ion ion-compose"></i>&nbsp;&nbsp;Update</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

<div class="modal" id="modal-info">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">Modal title</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
</script>
@endsection
