@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>Rekap
</h1>
@endsection
@section('title', 'List')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Req</th>
                <th>Keperluan</th>
                <th>Lama Balik Nota</th>
                <th>Prioritas</th>
                <th>TF/Cash</th>
                <th>Balik Nota/Cash</th>
                <th>Sisa</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ number_format($d->nominal) }}</td>
                    <td>{{ $d->keperluan }}</td>
                    <td>{{ $d->lama_balik_nota }} Hari</td>
                    <td>{{ $d->prioritas }}</td>
                    <td>{{ number_format($d->tf_finance) }}</td>
                    <td>{{ number_format($d->nominal_nota) }}</td>
                    <td>{{ number_format($d->tf_finance-$d->nominal_nota) }}</td>
                    <td>{{ $d->step }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

<div class="modal" id="modal-info">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">Modal title</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('#datatables').dataTable();
        $('#datatables_wrapper .table-caption').text('Some header text');
        $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });
</script>
@endsection
