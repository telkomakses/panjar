@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>{{ $data ? 'Request Panjar' : 'Edit Panjar' }}</span>
</h1>
@endsection
@section('title', $data ? 'Request' : 'Edit')
@section('content')
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data" action="/saveRequestPanjar/{{ Request::segment(2) ?: '0' }}">
    <div class="form-group">
        <label for="keperluan" class="col-md-2 control-label">Keperluan</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="keperluan" id="keperluan" class="form-control" required value="{{ @$data->keperluan }}" />
        </div>
        <label for="nominal" class="col-md-1 control-label">Nominal</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="nominal" id="nominal" class="form-control" required value="{{ @$data->nominal }}"/>
        </div>
        <label for="lama_balik_nota" class="col-md-2 control-label">Lama Balik Nota (hari)</label>
        <div class="col-md-1 form-message-dark">
            <input type="text" name="lama_balik_nota" id="lama_balik_nota" class="form-control" required value="{{ @$data->lama_balik_nota }}"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
    });
</script>
@endsection
