@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Cash & Bank</span>
</h1>
@endsection
@section('title', 'Cash & Bank')
@section('content')
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data" action="/save_tf/{{ Request::segment(2) }}">
    <div class="form-group form-message-dark">
        <label class="col-md-2 control-label">Request</label>
        <div class="col-md-9">
            <input type="text" class="form-control" value="{{ number_format($data->nominal) ?: '' }}" disabled/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tf_finance" class="col-md-2 control-label">Transfer/Cash</label>
        <div class="col-md-9">
            <input type="text" name="tf_finance" id="tf_finance" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-2 control-label">Catatan</label>
        <div class="col-md-9">
            <textarea name="catatan" id="catatan" class="form-control" required></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        // $('#upload_lop').pxFile();
        $('#status').select2({
            placeholder:'Select Status',
            data:[{'id':'4','text':'Approve'},{'id':'2','text':'Reject'},{'id':'99','text':'Cancel'}]
        });
    });
</script>
@endsection
