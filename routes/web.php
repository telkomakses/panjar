<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/login', function () {
  return view('login');
});
Route::post('/login', 'UserController@login');
Route::group(['middleware' => 'tomman.auth'], function () {
  Route::get('/', 'HomeController@home');
  Route::get('/rekap', 'HomeController@rekap');
  Route::get('/logout', 'UserController@logout');

  Route::get('/list/{step_id}', 'PanjarController@list');
  Route::get('/requestPanjar', 'PanjarController@create_panjar');
  Route::get('/progress/{id}', 'PanjarController@progress');

  Route::post('/requestPanjar', 'PanjarController@save_request_panjar');
  Route::post('/saveRequestPanjar/{id}', 'PanjarController@save_request_panjar');
  Route::post('/save_approval/{id}', 'PanjarController@save_approval');
  Route::post('/save_tf/{id}', 'PanjarController@save_tf');
  Route::post('/save_balik_nota/{id}', 'PanjarController@save_balik_nota');
});
